# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
def grab_image(movie,url,name)
    downloaded_image = URI.parse(url).open
	movie.photo.attach(io: downloaded_image, filename: name)
	movie.image_link=Rails.application.routes.url_helpers.rails_blob_path(movie.photo, only_path: true)
	movie.save
end
movie=Movie.create(name: "Just go with it", position: 1, image_link: "")
grab_image(movie, "https://m.media-amazon.com/images/M/MV5BMTM3MzM3NDE5MV5BMl5BanBnXkFtZTcwNDE5MTUxNA@@._V1_FMjpg_UX1000_.jpg", "movie1.jpg") 
movie=Movie.create(name: "Pirates of the Caribbean", position: 2, image_link: "")
grab_image(movie, "https://m.media-amazon.com/images/I/91fPdJwTMBL._SL1500_.jpg", "movie2.jpg") 
movie=Movie.create(name: "Shrek", position: 3, image_link: "")
grab_image(movie, "https://pics.filmaffinity.com/Shrek-903764423-large.jpg", "movie3.jpg")
movie=Movie.create(name: "Spider-Man", position: 4, image_link: "")
grab_image(movie, "https://images.moviesanywhere.com/e84b2c6e0de5278f8a00a8fedf73d60b/367910a3-05da-4ad5-8ef3-317708a1ca48.jpg", "movie4.jpg")
movie=Movie.create(name: "The dark knight", position: 5, image_link: "")
grab_image(movie, "https://m.media-amazon.com/images/I/91KkWf50SoL._SL1500_.jpg", "movie5.jpg")

